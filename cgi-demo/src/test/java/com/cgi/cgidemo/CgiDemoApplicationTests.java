package com.cgi.cgidemo;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CgiDemoApplicationTests {

	@Test
	void test1() {
		assertFalse(false, "OPSS!!(1)");
	}

	@Test
	void test2() {
		assertFalse(false, "OPSS!!(2)");
	}

	@Test
	void test3() {
		assertFalse(false, "OPSS!!(3)");
	}

}
