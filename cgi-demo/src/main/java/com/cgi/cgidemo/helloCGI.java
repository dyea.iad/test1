package com.cgi.cgidemo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class helloCGI {
    @GetMapping("/cgi")
    public String cgi() {
		return "Greetings from CGI API cococo";
	}
}
