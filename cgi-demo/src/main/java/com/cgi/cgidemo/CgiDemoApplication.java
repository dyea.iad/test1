package com.cgi.cgidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@RestController
public class CgiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CgiDemoApplication.class, args);
	}

	@GetMapping("/")
	public String home() {
		return "Hello CGI World";
	}

}
